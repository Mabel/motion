///**
// * Created by MBENBEN on 2016/9/20.
var mysql = require('mysql');
//var $conf = require('../conf/db');
//var $util = require('../util/util');
var $sql = require('./../petServer/userSqlMapping');
var $conf = require('../conf/db');
var conn = mysql.createConnection($conf.mysql);
conn.connect();

//var insertSQL = 'insert into t_user(name) values("conan"),("fens.me")';
//var selectSQL = 'select * from t_user limit 10';
//var deleteSQL = 'delete from t_user';
//var updateSQL = 'update t_user set name="conan update"  where name="conan"';

//delete
//conn.query(deleteSQL, function (err0, res0) {
//    if (err0) console.log(err0);
//    console.log("DELETE Return ==> ");
//    console.log(res0);
//
//    //insert
//    conn.query(insertSQL, function (err1, res1) {
//        if (err1) console.log(err1);
//        console.log("INSERT Return ==> ");
//        console.log(res1);
//
//        //query
//        conn.query(selectSQL, function (err2, rows) {
//            if (err2) console.log(err2);
//
//            console.log("SELECT ==> ");
//            for (var i in rows) {
//                console.log(rows[i]);
//            }
//
//            //update
//            conn.query(updateSQL, function (err3, res3) {
//                if (err3) console.log(err3);
//                console.log("UPDATE Return ==> ");
//                console.log(res3);
//
//                //query
//                conn.query(selectSQL, function (err4, rows2) {
//                    if (err4) console.log(err4);
//
//                    console.log("SELECT ==> ");
//                    for (var i in rows2) {
//                        console.log(rows2[i]);
//                    }
//                });
//            });
//        });
//    });
//});
// */
//// dao/userDao.js
//// 实现与MySQL交互
//var mysql = require('mysql');
//var $conf = require('../conf/db');
//var $util = require('../util/util');
//var $sql = require('./userSqlMapping');
//
//// 使用连接池，提升性能
//var pool  = mysql.createPool($util.extend({}, $conf.mysql));
//
//// 向前台返回JSON方法的简单封装
var jsonWrite = function (res, ret) {
    if(typeof ret === 'undefined') {
        res.json({
            code:'1',
            msg: '操作失败'
        });
    } else {
        res.json(ret);
    }
};

module.exports = {
    add: function (req, res, next) {
      //  pool.getConnection(function(err, connection) {
            // 获取前台页面传过来的参数
            var param = req.query || req.params;

            // 建立连接，向表中插入值
            // 'INSERT INTO user(id, name, age) VALUES(0,?,?)',
            conn.query($sql.add, [param.name, param.pas], function(err, result) {
                if(result) {
                    result = {
                        code: 200,
                        msg:'增加成功'
                    };
                }

                // 以json形式，把操作结果返回给前台页面
                jsonWrite(res, result);

                // 释放连接
                conn.end();
            });
       // });
    }
};
