///**
// * Created by MBENBEN on 2016/9/20.

var mysql = require('mysql');
var $sql = require('./userSqlMapping');
var $conf = require('../conf/db');
var conn = mysql.createPool($conf.mysql);

//var mysql = require('easymysql');
//var $sql = require('./userSqlMapping');
//var $conf = require('../conf/db');
//
//var conn = mysql.create({
//    'maxconnections' : 100
//});
//
//conn.addserver($conf.mysql);
//
//conn.on('busy', function (queuesize, maxconnections, which) {
//    console.log('busy')
//    // XXX: write log and monitor it
//});

var jsonWrite = function (res, ret) {
    if(typeof ret === 'undefined') {
        res.json({
            code:'1',
            msg: '参数有误'
        });
    } else {
        res.json(ret);
    }
};

module.exports = {
	addReview:function(req,res,next){
        var param = req.body;
        conn.getConnection(function(err,conn){
            conn.query($sql.updateReview, [param.id], function(err, result) {
                //console.log(param,param.number,new Date())
                if(result) {
                        jsonWrite(res, {success: true, msg: '阅读加一'});
                         conn.release();

                }else{
                    jsonWrite(res, {success: false, msg: '添加失败'},err);
                }
            });
        })

    },
    addPower:function(req,res,next){
        var param = req.body;
        conn.getConnection(function(err,conn){
            conn.query($sql.updatePower,[param.id],function(err, result) {
                if(result) {

                        jsonWrite(res, {success: true, msg: '点赞加一'});

                }else{
                    jsonWrite(res, {success: false, msg: '添加失败'},err);
                }
            });
        })

    },
    add:function(req,res,next){
        var param = req.query || req.params;
        conn.getConnection(function(err,conn){
            conn.query($sql.addInfo, ['0','0',new Date()], function(err, result) {
                //console.log(param,param.number,new Date())
                if(result) {
                    conn.query($sql.selectLast,{}, function(err, result) {
                        //    console.log(result)
                        jsonWrite(res, {success: true, msg: '主题添加成功',id:result[0].id});
                         conn.release();
                    })
                }else{
                    jsonWrite(res, {success: false, msg: '添加失败'},err);
                }
            });
        })

    },
    getList:function(req,res,next){
        var param = req.query || req.params
        // conn.connect();
        //console.log(param.stauts)
        conn.getConnection(function(err,conn) {
            conn.query($sql.getList, [param.stauts], function (err, result) {
               // console.log(result)
                if (result) {
                    jsonWrite(res, {success: true, msg: '列表获取成功', data: result});
                    //   conn.end();
                    conn.release();
                } else {
                    jsonWrite(res, {success: false, msg: '列表获取失败'});
                    // conn.end();
                    conn.release();
                }
            });
        })
    },
    
    getInfo:function(req,res,next){
        var param = req.query || req.params;
        //console.log(req.query,req.params,req.body )
        // conn.connect();
        conn.getConnection(function(err,conn) {
            conn.query($sql.getInfo, [param.id], function (err, result) {
                // console.log(result)
                if (result) {
                    jsonWrite(res, {success: true, msg: '信息获取成功', data: result});
                    //   conn.end();
                    conn.release();
                } else {
                    jsonWrite(res, {success: false, msg: '信息获取失败'});
                    // conn.end();
                    conn.release();
                }
            });
        })
    },
    updateInfo:function(req,res,next){
        var param = req.body;//req.query || req.params||
      //  console.log(req.query,req.params,req.body )
        // conn.connect();
        conn.getConnection(function(err,conn) {
            param.update = new Date()
            //console.log(param)

            conn.query($sql.updateInfo, [param.title, param.content, new Date(), param.stauts,param.author,param.g_name,param.g_url,parseInt(param.review),parseInt(param.power),parseInt(param.id)], function (err, result) {
                //console.log(err)
                if (result) {
                    jsonWrite(res, {success: true, msg: '保存成功'});
                    //   conn.end();
                    conn.release();
                } else {
                    jsonWrite(res, {success: false, msg: '保存失败'});
                    // conn.end();
                    conn.release();
                }
            });
        })
    },
    remove:function(req,res,next){
        var param = req.body;//req.query || req.params||
        // console.log(req.query,req.params,req.body )
        // conn.connect();
        conn.getConnection(function(err,conn) {
            param.update = new Date()
            //console.log(param)

            conn.query($sql.remove, [ new Date(), param.id], function (err, result) {
                //console.log(result)
                if (result) {
                    jsonWrite(res, {success: true, msg: '删除成功'});
                    //   conn.end();
                    conn.release();
                } else {
                    jsonWrite(res, {success: false, msg: '删除失败'});
                    // conn.end();
                    conn.release();
                }
            });
        })
    },
    intoPetData:function(req,res,next){
        var param = req.query || req.params;

       // jsonWrite(res, {success: false, msg: '�û����Ѵ��ڣ�'});
        //conn.query($sql.intoPetData, [param.name], function(err, result) {
        //    console.log(result)
        //    if(result) {
        //        console.log('ͬ�����û��м���', result[0].account)
        //        if (result.length > 0) {
        //            jsonWrite(res, {success: false, msg: '�û����Ѵ��ڣ�'});
        //            // �ͷ�����
        //            conn.end();
        //        }else{
        //            conn.query($sql.register, [param.name, param.pas,new Date()], function(err, result) {
        //                if(result) {
        //                    result = {
        //                        success: true,
        //                        msg:'ע��ɹ�'
        //                    };
        //                }
        //
        //                // ��json��ʽ���Ѳ���������ظ�ǰ̨ҳ��
        //                jsonWrite(res, result);
        //
        //                // �ͷ�����
        //                conn.end();
        //            });
        //        }
        //    }
        //
        //    // ��json��ʽ���Ѳ���������ظ�ǰ̨ҳ��
        //
        //});
    },
    login:function(req,res,next){
        var param = req.body || req.params
        // console.log(req.query,req.params,req.body )
        conn.getConnection(function(err,conn) {
            conn.query($sql.login, [param.loginName,param.password], function (err, result) {
                //   console.log(result)
                if (result.length>0) {
                    jsonWrite(res, {success: true, msg: '登录成功',data:result});
                    conn.release();
                } else {
                    jsonWrite(res, {success: false, msg: '用户名或密码错误！'});
                    conn.release();
                }
            });
        })
    },
    register: function (req, res, next) {
        //  pool.getConnection(function(err, connection) {
        // ��ȡǰ̨ҳ�洫�����Ĳ���
       // console.log(res,next)
        var param = req.query || req.params;

        // �������ӣ�����в���ֵ

        conn.query($sql.selectUser, [param.name], function(err, result) {
            console.log(result)
            if(result) {
                console.log('ͬ�����û��м���', result[0].account)
                if (result.length > 0) {
                    jsonWrite(res, {success: false, msg: '�û����Ѵ��ڣ�'});
                    // �ͷ�����
                    conn.end();
                }else{
                    conn.query($sql.register, [param.name, param.pas,new Date()], function(err, result) {
                        if(result) {
                            result = {
                                success: true,
                                msg:'ע��ɹ�'
                            };
                        }

                        // ��json��ʽ���Ѳ���������ظ�ǰ̨ҳ��
                        jsonWrite(res, result);

                        // �ͷ�����
                        conn.end();
                    });
                }
            }


        });

    }
};

function getDate(strDate){
    var date = eval('new Date(' + strDate.replace(/\d+(?=-[^-]+$)/,
            function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');
    return date;
}
