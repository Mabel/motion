/**
 * Created by MBENBEN on 2016/9/20.
 */
// dao/userSqlMapping.js
// CRUD SQL语句
var user = {
    addInfo:"INSERT INTO cha_article(stauts,number,createdate) VALUES(?,?,?) ",
    selectLast:'select id from cha_article where id=(select max(id) from cha_article)',
    getList:'select id,title from cha_article where stauts=?',
    getInfo:'select * from cha_article where id =?',
    updateInfo:'update cha_article set title=?,content=?,UpdateDate=?,stauts=?,author=?,g_name=?,g_url=?,review=?,power=? where id=?',
    updateReview:'update cha_article set review=review+1 where id=?',
    updatePower:'update cha_article set power=power+1 where id=?',
    remove:'update cha_article set stauts=1,UpdateDate=? where id =?',
    register:"INSERT INTO user(account, password,createdate) VALUES(?,?,?)",
    intoPetData:'INSERT INTO user(account, password,createdate) VALUES(?,?,?)',
    delete: 'delete from user where id=?',
    login:'select username from user where username=? and password=?',
};
module.exports = user;
